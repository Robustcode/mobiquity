import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WorldChampionshipsComponent } from '../world-championships/world-championships.component';
import { ListModule } from '../../components/list/list.module';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [WorldChampionshipsComponent],
  imports: [
    CommonModule,
    ListModule,
    RouterModule.forChild([
      {
        path: 'world-championships',
        component: WorldChampionshipsComponent,
      },
      {
        path: 'world-championships/:year',
        component: WorldChampionshipsComponent,
      }
    ]),
  ]
})
export class WorldChampionshipsModule { }
