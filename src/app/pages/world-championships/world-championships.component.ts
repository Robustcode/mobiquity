import { Component, ElementRef, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { ErgastDeveloperService } from 'src/app/api/extended/ergast-developer.service';
import { ActivatedRoute } from '@angular/router';
import { WinnerDetails } from 'src/app/model/winnerDetails';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-world-championships',
  templateUrl: './world-championships.component.html',
  styleUrls: ['./world-championships.component.scss']
})
export class WorldChampionshipsComponent implements AfterViewInit {
  @ViewChild('card_expander', { static: false }) cardExpander!: ElementRef<HTMLElement>;
  public year: number;
  public yearList: number[] = [];
  public raceList: WinnerDetails[] = [];
  public loading: boolean = true;
  private raceSubscription: Subscription;
  private championSubscription: Subscription;
  private paramsSubscription: Subscription;

  constructor(
    public ergastDeveloperService: ErgastDeveloperService,
    public activatedRoute: ActivatedRoute
  ) {}

  /* I have decided to use ngAfterViewInit because
  if i try to fetch the year parameter from the
  constructer or ngOninit the menu component seems
  to not have loaded completely by the time my
  subscription happens.
  
  Here i'm subscribing to the activated route
  so i can retrieve the selected year */
  public ngAfterViewInit(): void {
    this.paramsSubscription = this.activatedRoute.paramMap.subscribe(params => {
      this.year = parseInt(params.get('year')) || new Date().getFullYear();
      this.getData(this.year);
    });
  }

  /* getData is where i'm subscribing to the different APIs that i
  i then use the respnses to generate a new list that i will pass
  on to my list component for display, i chose to wrap the getChampion
  service as that was quicker plus it was just two services but
  had it been more services i would have probaly used switchMap,
  forkJoin or something like that  */
  private getData(year: number): void {
    /* for the animated scroll i'm using view child and setting
    the list wrapper height to the scroll height and back to null
    before we fetch other items */
    const expandedCard = this.cardExpander?.nativeElement;
    expandedCard.style.maxHeight = null;
    this.loading = true;
    
    this.raceSubscription = this.ergastDeveloperService.getRaceTable(year).subscribe((raceTable: any) => {

      this.championSubscription = this.ergastDeveloperService.getChampion(year).subscribe((championTable: any) => {
        this.raceList = [];
        
        const championId = championTable.MRData.StandingsTable.StandingsLists[0].DriverStandings[0].Driver.driverId;
        
        this.raceList = raceTable?.MRData?.RaceTable?.Races?.map((race: any) => {
          const driverId = race.Results[0].Driver.driverId;
          const championStatus = (driverId === championId ? true : false);
          /* API responses are big and thefirst couple of
          instances have unnecessary information, i tried
          to use models to reduce the objects but that
          didn't workout so well so i dected to creat my
          own model which i can ad and remove instances
          depending on what i want to show in the view */
          return {
            champion: championStatus,
            points: race.Results[0].points,
            givenName: race.Results[0].Driver.givenName,
            familyName: race.Results[0].Driver.familyName,
            raceLocation: race.Circuit.circuitName
          }
        });

        const scrollHeight = expandedCard.scrollHeight != 0 ? `${expandedCard.scrollHeight}px` : `100%`;
        expandedCard.style.maxHeight = scrollHeight;
        this.loading = !this.loading;
      });
    });
  }

  /* I have added ngOnDestroy so i 
  can unsubscribe to all my subscriptions */
  public ngOnDestroy(): void {
    this.raceSubscription.unsubscribe();
    this.championSubscription.unsubscribe();
    this.paramsSubscription.unsubscribe();
  }
}
