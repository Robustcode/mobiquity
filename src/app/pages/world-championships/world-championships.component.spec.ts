import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WorldChampionshipsComponent } from './world-championships.component';

describe('WorldChampionshipsComponent', () => {
  let component: WorldChampionshipsComponent;
  let fixture: ComponentFixture<WorldChampionshipsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WorldChampionshipsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WorldChampionshipsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
