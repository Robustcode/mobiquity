import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WorldChampionshipsComponent } from './pages/world-championships/world-championships.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'world-championships',
    pathMatch: 'full'
  },
  {
    path: 'world-championships',
    component: WorldChampionshipsComponent
  },
  {
    path: 'world-championships',
    loadChildren: () =>
      import('./pages/world-championships/world-championships.module').then(m => m.WorldChampionshipsModule),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
