import { Component, Input, OnInit } from '@angular/core';
import { Router, RoutesRecognized } from '@angular/router';
import { generateYearList } from 'src/app/utilities/generateYearList';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
  @Input() private startYear: number;
  public yearList: number[] = [];
  public year: number;

  constructor(private router: Router) {}

  public ngOnInit(): void {
    this.yearList = generateYearList(this.startYear);
    /* On load of the app if there is year parameter
    value it's not picking up the value so i decided
    to subscribe to the router events and fetch the
    value which i will use through out */
    this.router.events.subscribe((data) => {
      if (data instanceof RoutesRecognized) {
        this.year = parseInt(data.state.url.split('/').pop()) || new Date().getFullYear();
      }
    });
  }

}
