import { Component, Input } from '@angular/core';
import { WinnerDetails } from 'src/app/model/winnerDetails';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent {
  /* All i have here is the input varible
  which retrives the data from the parent
  componet, this is just for the list view
  so not much needed but scss and html and
  the iternation of the data */
  @Input() public raceList: WinnerDetails[] = [];

  constructor() { }
}
