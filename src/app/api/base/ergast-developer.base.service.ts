import { ErgastDeveloperApiService } from '../ergast-developer-api.service';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { MRData } from 'src/app/model/MRData';

@Injectable({ providedIn: 'root' })
export class ErgastDeveloperBaseService {
  constructor(protected api: ErgastDeveloperApiService) {}

  public getStandingsTable(season: number): Observable<MRData> {
    return this.api.get<MRData>(
      `/f1/${season}/driverStandings.json`,
    );
  }

  public getRaceTable(season: number): Observable<MRData> {
    return this.api.get<MRData>(
      `/f1/${season}/results/1.json`,
    );
  }

  public getChampion(season: number): Observable<MRData> {
    return this.api.get<MRData>(
      `/f1/${season}/driverStandings/1.json`,
    );
  }
}
