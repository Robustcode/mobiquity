import { ErgastDeveloperBaseService } from '../base/ergast-developer.base.service';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class ErgastDeveloperService extends ErgastDeveloperBaseService {}
