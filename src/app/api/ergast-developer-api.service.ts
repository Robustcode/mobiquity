import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class ErgastDeveloperApiService {
  private base: string = environment.api;

  constructor(private http: HttpClient) {}

  public get<T>(url: any): Observable<T> {
    return this.http.get<T>(`${this.base}${url}`);
  }
}
