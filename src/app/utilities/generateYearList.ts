/* generateYearList() is to generate a list of years
from a specified year to current */
export function generateYearList(startYear: number) {
    const currentYear: number = new Date().getFullYear();
    let years: number[] = [];
    startYear = startYear || 2005;  
    while ( startYear <= currentYear ) {
        years.push(startYear++);
    }   
    return years.sort((a, b) => b - a);
}