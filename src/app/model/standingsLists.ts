import { DriverStandings } from "./driverStandings";

export class StandingsLists {
    public season: number;
    public round: number;
    public DriverStandings: DriverStandings[];
}