import { StandingsLists } from "./standingsLists";

export class StandingsTable {
    public season: number;
    public StandingsLists: StandingsLists[];
}