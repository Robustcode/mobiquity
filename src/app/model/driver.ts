export class Driver {
    public driverId: string;
    public givenName: string;
    public familyName: string;
    public dateOfBirth: Date;
    public nationality: string;
}