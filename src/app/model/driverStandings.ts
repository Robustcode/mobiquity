import { Driver } from "./driver";
import { Constructors } from "./constructors";

export class DriverStandings {
    public position: number;
    public points: number;
    public wins: number;
    public Driver: Driver[];
    public Constructors: Constructors[];
    public champion: boolean;
}