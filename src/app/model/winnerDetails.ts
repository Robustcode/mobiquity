export class WinnerDetails {
    public champion: boolean;
    public points: number;
    public givenName: string;
    public familyName: string;
    public raceLocation: Date;
}