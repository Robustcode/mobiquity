# Mobiquity

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.2.0.

## Installing dependencies

Make sure you have angular-cli `npm install -g @angular/cli` and nodejs `nodejs.org` installed

Clone and open the project with your preferred IDE like vs-code then run `npm install` to install all the app dependencies...
## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## About the App

'I have created a SPA that presents a list that shows the F1 world champions starting from 2005 until now. Clicking on an item from the horizontal scrolling list shows the list of the winners for every race for the selected year. It also highlights the row when the winner has been the world champion in the same season. A quick view of the app can be found on this lint `mobiquity.vercel.app`'

1. I have broken down the app into three components, the menu, list and world-champion page, doing so helps manage the project a little easier as the code doesn't get too lengthy like it would when it's just in one component.

2. To identify the selected season i'm using the routerLink to update a url parameter that i have set on the world-champions page, i can now access the route varible in any page or component of my choosing by subscribing to activeRoute params. RouterLink helps me respect the SPA principle as the main UI loads once and the list view loads when it's needed. (on first load there will most likely be no season number set on the parameter so to fix that if there param returns null i'm manually setting it to find the latest season which will be the current year)

3. I'm using two services in the app, one to identify all the winners for the different races and one to identify the world champion for the particular season so i can highlight the row.

* I had thought of implementing localStorage to save the data on every selection of the season, so that every time a season is clicked i would first check if the item for that season exist, if it does exist i would fetch from there else i would subscribe to the API, that idea was based on saving the user some resources since previously visited season would be fetch from their device, i would have however added an expiry date so that the app can fetch new items if there are any changes to the results. I wrote some of it but ran into some issues and time wasn't on my side so i parked the idea on a branch instead. 
